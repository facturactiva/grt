# GUIA DE REMISION TRANSPORTISTA [30]

> (...) en los json indica que puede existir mas campos a nivel del nodo.

## CAMPOS QUE COMPONEN Y ESTRUCTURA
### Datos de guía de remisión

CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Numeracion, conformada por serie y Número correlativo | an..13 | V###-NNNNNNNN
Fecha de emisión|an10|YYYY-MM-DD
Hora de emisión|an8|hh:mm:ss
Observaciones |an..250|

```json
{
    "fechaEmision": "2022-11-30",
    "documento": {
        "serie": "V001",
        "correlativo": 1,
        ...
    }
    "datosAdicionales": {
        "observacion": "Emision guia transportista"
        ...
    }
    ...
}
````
### Datos del Transportista

CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Tipo y número de documento de identidad del transportista | an11|n11|
Apellidos y Nombres o denominacion o razón social del transportista | an..250|
Número de Registro MTC | an..20|
Número de autorización | an..50|
Código de entidad autorizadora | an2 |(Catálogo N° D-37)|

```json
{
    ...
    "transportista": {
        "tipoDocumento": "6",
        "numeroDocumento": "20609627027",
        "mtcCod": "123456",
        "numeroAutorizacion": "111111",
        "entidadAutorizadoraCod": "11",
    },
}
```

### Documento relacionado - solo se permite un documento, excepto hasta 2 si se ingresa alguno de los códigos "31", "65", "66", "67", "68", "69", o más de 2 si se ingresa código "09" con serie electrónica
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Código del tipo de documento relacionado | an2|(Catálogo N° 61)
Número de documento relacionado | an..100
Número de RUC del emisor del documento relacionado | an11 | n11
```json
{
    "referencia": [
         {
            "tipoDocumentoRef": "03",
            "numeroDocumentoRef": "B001-123",
            "rucEmisorDocumentoRef": "20609627027"
        }
    ],
    ...
}
```

### Datos del Remitente
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Tipo de documento de identidad  |an1|(Catálogo N° 06)
Número de documento de identidad |an..15
Apellidos y nombres, denominacion o razón social del remitente |an..250
``` json
{
    "documento": {
        "nombreEmisor": "SOLEIL GAS S.A.C.",
        "tipoDocEmisor": "6",
        "numDocEmisor": "20609627027",
        ...
    },
    ...
}
```

### Datos del Destinatario
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Tipo de documento de identidad |an1|(Catálogo N° 06)
Número de documento de identidad |an..15
Apellidos y nombres, denominacion o razón social del destinatario |an..250
```json
{
    "documento": {
        "nombreReceptor": "FACTURACTIVA DEL PERU S.A.C. - FACTURACTIVA S.A.C.",
        "tipoDocReceptor": "6",
        "numDocReceptor": "20600543050",
        ...
    },
}
```

### Bienes a transportar
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Cantidad | an..23|n(12,10)
Unidad de medida | an..3|(Catálogo N.° 03)(Catálogo N.° 65)
Nombre del item | an..500
Código del item | an..30
Código producto SUNAT | n..8|(Catálogo N.° 25)
Código GTIN | an..14
idOperacion|an80|

```json
{
    "detalle": [
        {
            "cantidadItem": 1,
            "unidadMedidaItem": "GLL",
            "nombreItem": "0001 - marticula",
            "codItem": "0001",
            "codItemUnspsc": "00000000000000",
            "codGTIN": "123456",
            "idOperacion": "4776-T001-35-1"
        }
    ],
    ...
}
```

### Dirección del punto de partida
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Ubigeo de partida |n6|(Catálogo N° 13)
Direccion completa y detallada de partida |an..500
Punto de georreferencia de partida(Longitud) |an..13|n(3,8)
Punto de georreferencia de partida(Latitud) |an..13|n(3,8)

```json
{
    "guia": {
        "direccionOrigen": "Jr. Franklin Roosevelt 837 INT 14-A",
        "direccionOrigenUbigeo": "150101",
        "direccionOrigenDistrito": "LIMA",
        "direccionOrigenProvincia": "LIMA",
        "direccionOrigenDepartamento": "LIMA",
        "longitudOrigen": 1,
        "latitudOrigen": 1,
        ...
    },
    ...
}
```
### Dirección del punto de llegada
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Ubigeo de llegada |n6|(Catálogo N° 13)
Direccion completa y detallada de llegada |an..500
Punto de georreferencia de llegada(Longitud) |an..13|n(3,8)
Punto de georreferencia de llegada(Latitud) |an..13|n(3,8)
```json
{
    "guia": {
        "direccionDestino": "Jr. Franklin Roosevelt 900",
        "direccionDestinoUbigeo": "150101",
        "direccionDestinoDistrito": "LIMA",
        "direccionDestinoProvincia": "LIMA",
        "direccionDestinoDepartamento": "LIMA",
        "longitudDestino": 1,
        "latitudDestino": 1,
        ...
    }
    ...
}
```

### Vehículos (El primero es principal, y el resto secundario hasta 2)
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Número de placa del vehiculo | an..8
Tarjeta Única de Circulación Electrónica o Certificado de Habilitación vehicular | an..15
Número de autorización | an..50
Entidad que emite el documento an2 | (Catálogo N° D-37)]

```json
{
    "vehiculos": [
        {
            "placa": "0611UD",
            "tarjetaCirculacion": "1231231234",
            "numAutorizacion": "123123123",
            "EntidadEmitDoc": "01"
        }
    ],
    ...
}
```

### Conductores (El primero es principal, y el resto secundario hasta 2)
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Tipo de documento de identidad |an1|(Catálogo N° 06)
Número de documento de identidad | an..15
Nombres | an..250
Apellidos | an..250
Número de licencia de conducir | an..10

```json
{
    "conductores": [
        {
            "tipoDoc": "1",
            "numDoc": "72526346",
            "nombres": "JOSE RODOLFO",
            "apellidos": "SALAS MONTA&#209;EZ",
            "licenciaConducir": "VM72526346"
        }
    ],
    ...
}
```

### Datos del traslado
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Fecha Inicio de traslado | an..10|YYYY-MM-DD
Peso bruto total de la carga | an..16|n(12,3)
Unidad de medida del peso bruto @unitCode | an3|"KGM" Kilogramos "TNE" Toneladas
Tipo de evento | an1|1-Transbordo no programado 2-Imposibilidad arribo 3-Imposibilidad entrega

```json
{
    "guia": {
        "fechaInicioTraslado": "2022-11-29",
        "totalPesoBrutoTransportado": 1000,
        "unidadMedidaPesoTransportado": "KGM",        
        "tipoEvento": "1",
        ...
    },
    ...
}
```

### Datos de empresa que subcontrata
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Número de RUC | an11|n11 
Apellidos y nombres, denominacion o razón social del subcontratador | an..250

```json
{
    "guia": {
        "rucSubContrata": "20725263461",
        "nombreSubContrata": "jose salas",
        ...
    },
    ...
}
```

### Datos quien paga el servicio de transporte - solo se completa cuando paga un tercero
CAMPO  | TIPO Y LONGITUD | FORMATO
------------- | :-------------: | :-------------:
Tipo de documento de identidad | an1|(Catálogo N° 06)
Número de documento de identidad | an..15
Apellidos y nombres, denominacion o razón social de quien paga el servicio | an..250

```json
{
    "guia": {
        ...
        "nombreTercero": "SOLEIL GAS S.A.C.",
        "tipoDocTercero": "6",
        "numDocTercero": "20609627027",
    },
    "indicadores": {
        "pagoPorTercero": true,
        ...
    },
}
```



Realiza transbordo programado, si es True pide direccion de Origen y FIN [LISTO]

autorizacionCod y entidadAutorizadoraCod son opcionales [LISTO]

CUANDO SE PONE CHECK SUBCONTRATO - Datos de empresa que subcontrata es requerido [LISTO]

VALIDACIONES:
fechaInicioTraslado >= fechaEmision

```json
{
    "tipoDocumento": "31",
    "fechaEmision": "2022-11-29",
    "idTransaccion": "31-20609627027-20221109-V002-120",
    "guia": {
        "horaEmision": "10:34:56",
        "codigoMotivoTraslado": "14",
        "direccionOrigen": "Jr. Franklin Roosevelt 837 INT 14-A",
        "direccionOrigenUbigeo": "150101",
        "longitudOrigen": 1,
        "latitudOrigen": 1,
        "direccionDestino": "Jr. Franklin Roosevelt 900",
        "direccionDestinoUbigeo": "150101",
        "longitudDestino": 1,
        "latitudDestino": 1,

        "fechaInicioTraslado": "2022-11-29",
        "totalPesoBrutoTransportado": 1000,
        "unidadMedidaPesoTransportado": "KGM",        
        "tipoEvento": "1",

        "rucSubContrata": "20725263461",
        "nombreSubContrata": "jose salas",

        
        "nombreTercero": "SOLEIL GAS S.A.C.",
        "tipoDocTercero": "6",
        "numDocTercero": "20609627027",
    },
    "indicadores": {
        "esTransbordoProgramado": false,
        "retornoVehiculoEnvaseVacio": true,
        "retornoVehiculoVacio": true,
        "transporteSubcontratado": true,
        "pagoPorTercero": false,
    },
    "referencia": [
         {
            "tipoDocumentoRef": "03",
            "numeroDocumentoRef": "B001-123",
            "rucEmisorDocumentoRef": "20609627027"
        }
    ],
    "transportista": {
        "tipoDocumento": "6",
        "numeroDocumento": "20609627027",
        "mtcCod": "123456",
        "autorizacionCod": "111111",
        "entidadAutorizadoraCod": "11",
    },
    "vehiculos": [
        {
            "placa": "0611UD",
            "tarjetaCirculacion": "1231231234",
            "numAutorizacion": "123123123",
            "EntidadEmitDoc": "01"
        }
    ],
    "conductores": [
        {
            "tipoDoc": "1",
            "numDoc": "72526346",
            "nombres": "JOSE RODOLFO",
            "apellidos": "SALAS MONTA&#209;EZ",
            "licenciaConducir": "VM72526346"
        }
    ],
    "documento": {
        "serie": "V002",
        "correlativo": 120,
        "nombreEmisor": "SOLEIL GAS S.A.C.",
        "tipoDocEmisor": "6",
        "numDocEmisor": "20609627027",
        "nombreReceptor": "FACTURACTIVA DEL PERU S.A.C. - FACTURACTIVA S.A.C.",
        "tipoDocReceptor": "6",
        "numDocReceptor": "20600543050",
        "tipoFormatoRepresentacionImpresa": "GENERAL"
    },
    "detalle": [
        {
            "cantidadItem": 1,
            "unidadMedidaItem": "GLL",
            "codItem": "0001",
            "nombreItem": "0001 - marticula",
            "idOperacion": "4776-T001-35-1"
        }
    ],
    "datosAdicionales": {
        "hora": "22:07:25",
        "fecha": "09/11/2022",
        "cajero": "jpaz@facturactiva.com",
        "puntoEmision": "FACTURACTIVA DEL PERU PRUEBAS",
        "detalle": [
            {
                "secuencial": 1,
                "cantidadItem": 1,
                "unidadMedidaItem": "GLL",
                "codItem": "0001",
                "nombreItem": "0001 - marticula",
                "idOperacion": "4776-T001-35-1"
            }
        ],
        "observacion": "test 1",
        "turno": "MAÑANA"
    },
    "correoReceptor": "jsalas@facturactiva.com"
}
```
